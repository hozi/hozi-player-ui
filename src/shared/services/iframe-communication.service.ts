import { Injectable, NgZone } from "@angular/core";
import postRobot from 'post-robot'
import debounce from 'lodash/debounce';

export interface PostMessageEvent { data?: any }
export interface PostMessageListener { cancel(): void }

@Injectable({
    providedIn: 'root'
})
export class IframeCommunicationService {

    constructor(private zone: NgZone) { }

    isEmbededInIframe() {
        return window.self !== window.parent;
    }

    async sendToEmbeddingParent(eventName: string, data?: any) {
        if (this.isEmbededInIframe()) {
            await postRobot.send(window.parent, eventName, data);
        }
    }

    listen(eventName: string, callback: (event: PostMessageEvent) => any, debounceTime: number = 150): PostMessageListener {
        const debouncedCallback = debounce(callback, debounceTime);
        return postRobot.on(eventName, e => this.zone.run(() => debouncedCallback(e)));
    }
}