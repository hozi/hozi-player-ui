import { Injectable } from "@angular/core"
import { of } from "rxjs"
import { Channel } from "src/app/channels-tab/channels-tab.component"

@Injectable({
    providedIn: 'root'
})
export class ChannelService {

    get channels$() {
        return of(this.getAllChannels())
    }

    getChannelNameById(channelId: string) {
        return (this.getAllChannels().filter((channel) => channel.id == channelId)[0] as Channel).name;
    }

    getAllChannels() {
        return [
            {
                id: "1",
                name: 'חוזי/פיקוד דרום/יחידה 1/ערוץ 1'
            },
            {
                id: "2",
                name: 'חוזי/פיקוד דרום/יחידה 1/ערוץ 2'
            },
            {
                id: "3",
                name: 'חוזי/פיקוד דרום/יחידה 3/ערוץ 1'
            },
            {
                id: "4",
                name: 'חוזי/פיקוד מרכז/יחידה 3/ערוץ 2'
            },
            {
                id: "5",
                name: 'חוזי/פיקוד מרכז/יחידה 4/ערוץ 1'
            },
            {
                id: "6",
                name: 'חוזי/פיקוד צפון/יחידה 4/ערוץ 2'
            },
            {
                id: "7",
                name: 'חוזי/פיקוד צפון/יחידה 2/ערוץ 2'
            },
            {
                id: "8",
                name: 'חוזי/פיקוד צפון/יחידה 4/ערוץ 3'
            },
            {
                id: "9",
                name: 'חוזי/פיקוד צפון/יחידה 4/ערוץ 4'
            }
        ]
    }
}
