import { Injectable } from "@angular/core";
import { PlayableMedia } from "src/app/app.component";
import { ChannelService } from "./services/channels.service";

@Injectable({
    providedIn: 'root'
})
export class HoziUtils {

    constructor(private channelService: ChannelService) {
    }

    convertVideoIdToPlayableMedia(videoId: string) {
        const channelName = this.channelService.getChannelNameById(videoId);
        return {
            id: videoId, name: channelName
        } as PlayableMedia;
    }
}