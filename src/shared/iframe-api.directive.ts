import { Directive, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { DashPlayer } from "../app/embed-video/dash-player/dash-player.component";
import { IframeActions } from "./iframe-api-messages-actions";
import { IframeCommunicationService, PostMessageListener } from "./services/iframe-communication.service";

@Directive({ selector: 'dash-player[iframeApi]' })
export class PlayerIframeDirective implements OnInit, OnDestroy {

    private postmanHandlers: PostMessageListener[] = [];
    private onDestroy$ = new Subject()

    constructor(private hostPlayer: DashPlayer,
        private iframeCommunications: IframeCommunicationService) {
    }

    ngOnInit() {
        if (this.iframeCommunications.isEmbededInIframe()) {
            this.registerApiHandler();

            this.hostPlayer.onVideoStart.pipe(takeUntil(this.onDestroy$))
                .subscribe(() => this.sendToIfame(IframeActions.VIDEO_LOADED, this.hostPlayer.videoId));
        }
    }

    ngOnDestroy() {
        if (this.iframeCommunications.isEmbededInIframe()) {
            this.deregisterApiHandler();
        }
        this.onDestroy$.next();
    }

    private registerApiHandler() {
        this.postmanHandlers = [
            this.iframeCommunications.listen(IframeActions.PLAY, (event: any) => {
                if (this.hostPlayer) {
                    this.hostPlayer.play();
                }
            }),
            this.iframeCommunications.listen(IframeActions.STOP, (event: any) => {
                if (this.hostPlayer) {
                    this.hostPlayer.pause();
                }
            }),
            this.iframeCommunications.listen(IframeActions.SEEK, (event: any) => {
                if (this.hostPlayer) {
                    this.hostPlayer.seek(Number(event.data));
                }
            }),
            this.iframeCommunications.listen(IframeActions.REVERSE, (event: any) => {
                if (this.hostPlayer) {
                    this.hostPlayer.reverse();
                }
            }),
            this.iframeCommunications.listen(IframeActions.GET_DURATION, (event: any) => {
                if (this.hostPlayer) {
                    return this.hostPlayer.getDuration();
                }
            }),
            this.iframeCommunications.listen(IframeActions.SEEK_ABS_TIME, (event: any) => {
                if (this.hostPlayer) {
                    return this.hostPlayer.seekAbsTime(2);
                }
            })
        ]
    }

    private deregisterApiHandler() {
        this.postmanHandlers.forEach(handler => handler.cancel());
    }

    private sendToIfame(action: string, data: any) {
        if (this.iframeCommunications.isEmbededInIframe()) {
            this.iframeCommunications.sendToEmbeddingParent(action, data);
        }
    }
}