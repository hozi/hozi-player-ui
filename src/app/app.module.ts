import { NgModule } from "@angular/core";
import { AppComponent } from './app.component';
import { ChannelsTabComponent } from "./channels-tab/channels-tab.component";
import { DashPlayer } from "./embed-video/dash-player/dash-player.component";
import { EmbedVideo } from "./embed-video/embed-video.component";
import { VideoHeader } from "./embed-video/video-header/video-header.component";
import { SearchComponent } from "./search/search.component";
import { AngularMaterialModule } from './angular-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { PlayerIframeDirective } from "src/shared/iframe-api.directive";

@NgModule({
  declarations: [
    AppComponent, SearchComponent, ChannelsTabComponent, EmbedVideo, DashPlayer, VideoHeader, PlayerIframeDirective
  ],
  imports: [
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    PlayerIframeDirective
  ],
  providers: [],
  entryComponents: [SearchComponent],
  bootstrap: [AppComponent]
})
export class HoziAppModule {
}