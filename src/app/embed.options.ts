export class EmbedOptions {
    showFullUI = false;
    showVideoName = true;
    hideVideoControls = false;

    constructor(queryParams?: URLSearchParams) {

        if (queryParams) {
            this.showFullUI = JSON.parse(queryParams.get('showFullUI')) ?? this.showFullUI,
            this.showVideoName = JSON.parse(queryParams.get('showVideoName')) ?? this.showVideoName,
            this.hideVideoControls = JSON.parse(queryParams.get('hideVideoControls')) ?? this.hideVideoControls
        }
    }
}