import { Component, Input, OnInit } from "@angular/core";
import { HoziUtils } from "src/shared/utils";
import { PlayableMedia, SelectedMediaParams } from "../app.component";
import { EmbedOptions } from "../embed.options";

@Component({
    selector: 'embed-video',
    templateUrl: './embed-video.component.html',
    styleUrls: ['./embed-video.component.scss']
})
export class EmbedVideo implements OnInit {
    currentVideo: SelectedMediaParams = null;
    isLoading: boolean;
    isError = false;
    video: PlayableMedia;

    @Input()
    set selectedVideo(selctedVideo: SelectedMediaParams) {
        if (selctedVideo) {
            this.currentVideo = selctedVideo;
            this.reset();
            this.loadVideo();
        } else {
            this.reset();
        }
    }

    @Input() embedOptions: EmbedOptions;

    constructor(private hoziUtils: HoziUtils) {
    }

    ngOnInit() {
    }

    reset() {
        this.video = null;
        this.isLoading = false;
        this.isError = false;
    }

    loadVideo() {
        this.isLoading = true;

        try {
            this.video = this.hoziUtils.convertVideoIdToPlayableMedia(this.currentVideo.channelId)
        } catch (error) {
            this.isError = true;
        } finally {
            this.isLoading = false;
        }
    }

    handleVideoStart() {
        this.isError = false;
    }
}