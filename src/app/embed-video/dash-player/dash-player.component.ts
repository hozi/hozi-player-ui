import { AfterViewInit, Component, ElementRef, Input, NgZone, OnDestroy, Output, EventEmitter, ViewChild } from "@angular/core";
import { MediaPlayer, MediaPlayerClass } from 'dashjs';
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged, takeUntil, tap } from "rxjs/operators";
import { PlayableMedia } from "src/app/app.component";
import { EmbedOptions } from "src/app/embed.options";
import { IframeActions } from "src/shared/iframe-api-messages-actions";
import { IframeCommunicationService } from "src/shared/services/iframe-communication.service";

export const SECOND_AS_MILIS = 1000
// at this moment we have 1 video for all channels
export const MPD_URL = "../../assets/videos/video3/video2_dash.mpd";

@Component({
    selector: 'dash-player',
    templateUrl: './dash-player.component.html',
    styleUrls: ['./dash-player.component.scss']
})
export class DashPlayer implements OnDestroy, AfterViewInit {
    initialized: boolean = false;
    video$ = new Subject<PlayableMedia>();
    videoId: string;
    protected dashPlayer: MediaPlayerClass;
    private onDestroy$ = new Subject();
    private reverseFrameRate: number = 0.1
    private isReversed: boolean = false
    private intervalRewind: any;

    @ViewChild('videoElement', { static: false }) videoElement: ElementRef<HTMLVideoElement>;

    @Input()
    set video(video: PlayableMedia) {
        if (video) {
            this.video$.next(video);
            this.videoId = video.id
        }
    }

    @Input() embedOptions: EmbedOptions;
    @Input() startFrom: number;
    @Output() onVideoStart = new EventEmitter();

    constructor(private zone: NgZone, private iframeCommunications: IframeCommunicationService) { }

    ngOnDestroy() {
        this.dashPlayer.pause();
        this.dashPlayer.reset();
        this.dashPlayer = null;
        this.onDestroy$.next();
    }

    ngAfterViewInit() {
        this.video$.pipe(
            distinctUntilChanged(),
            takeUntil(this.onDestroy$),
            debounceTime(50)
        ).subscribe(newVideo => {
            this.zone.runOutsideAngular(() => {
                this.startPlayer(newVideo);
                this.publishVideoTimeEvents()
            })
        })
    }

    startPlayer(video: PlayableMedia) {
        if (video) {
            if (this.initialized) {
                this.retryPlayer();
            } else {
                this.initPlayer();
            }
        }
    }

    private retryPlayer() {
        this.dashPlayer.attachSource(MPD_URL);
    }

    private initPlayer() {
        if (this.initialized == true) {
            throw new Error("player already initialized");
        }

        this.initDash();
        this.initialized = true;
        this.onVideoStart.next();

        // waiting 100ms for the player to be trully initialized, not a good solution but
        // its fine for this project.
        if (this.startFrom) setTimeout(() => this.seek(this.startFrom), 100)
    }

    private initDash() {
        this.dashPlayer = MediaPlayer().create();
        this.dashPlayer.initialize();
        this.dashPlayer.updateSettings({
            streaming: {
                jumpGaps: true,
                smallGapLimit: 5
            }
        });
        this.dashPlayer.attachView(this.videoElement.nativeElement);
        this.dashPlayer.attachSource(MPD_URL);
        this.dashPlayer.setMute(true);
        this.dashPlayer.setAutoPlay(true);

        if (!this.embedOptions.hideVideoControls) {
            this.videoElement.nativeElement.setAttribute('controls', 'true');
        }
    }

    play() {
        this.dashPlayer.play();
    }

    pause() {
        this.dashPlayer.pause();
    }

    seek(time: number) {
        const fixedTime = Math.max(0, Math.min(this.getDuration(), time));
        this.dashPlayer.seek(fixedTime);
    }

    seekAbsTime(time: number) {
        //here we should convert the abs time recived to relative and seek to it
        const relativeAfterConversion = 2
        this.seek(relativeAfterConversion)
    }

    reverse() {
        this.isReversed = !this.isReversed;
        if (this.isReversed) {
            this.intervalRewind = setInterval(() => {
                this.dashPlayer.setPlaybackRate(0);
                if (this.videoElement.nativeElement.currentTime < this.reverseFrameRate) {
                    clearInterval(this.intervalRewind);
                    this.pause();
                } else {
                    this.videoElement.nativeElement.currentTime -= this.reverseFrameRate;
                }
            }, this.reverseFrameRate * SECOND_AS_MILIS)
        } else {
            clearInterval(this.intervalRewind);
            this.dashPlayer.setPlaybackRate(1);
        }
    }

    getDuration() {
        return this.dashPlayer.duration();
    }

    private publishVideoTimeEvents() {
        this.dashPlayer.on("playbackTimeUpdated", (({ time }) => {
            const eventToPublish = {
                sourceId: this.videoId,
                seekbarStartTime: 0,
                seekbarEndTime: this.getDuration(),
                currentTime: time,
            }
            this.iframeCommunications.sendToEmbeddingParent(IframeActions.VIDEO_TIME,eventToPublish);
        }))
    }
}