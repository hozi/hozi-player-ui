import { Component, EventEmitter, Output } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged, map, withLatestFrom } from 'rxjs/operators';
import { ChannelService } from "src/shared/services/channels.service";
import { SelectedMediaParams } from '../app.component';
import { Channel } from "../channels-tab/channels-tab.component";

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent {
    searchInput = new FormControl();
    channels$: Observable<Channel[]>
    @Output() onSelectResult = new EventEmitter<SelectedMediaParams>();

    constructor(private channelService: ChannelService) {}

    ngOnInit() {
        const allChannels$ = this.channelService.channels$;
        this.channels$ = this.searchInput.valueChanges.pipe(
            debounceTime(100),
            distinctUntilChanged(),
            withLatestFrom(allChannels$),
            map(([search, allChannels]) => allChannels.filter(channel => channel.name.includes(search)))
        )
    }

    onSearchResultClicked(channel: Channel) {
        this.onSelectResult.emit({ channelId: channel.id })
    }
}