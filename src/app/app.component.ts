import { Component, OnInit } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { HoziUtils } from "src/shared/utils";
import { IframeActions } from "../shared/iframe-api-messages-actions";
import { IframeCommunicationService } from "../shared/services/iframe-communication.service";
import { EmbedOptions } from "./embed.options";

type CurrentView = 'right' | 'middle';

export interface PlayableMedia {
    id?: string;
    name?: string;
}

export interface SelectedMediaParams {
    id?: string;
    channelId?: string;
    media?: PlayableMedia;
    startFrom?: number
}

@Component({
    selector: 'hozi-application',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    currentView$ = new BehaviorSubject<CurrentView>('middle');
    selectedVideo: SelectedMediaParams;
    embedOptions: EmbedOptions;

    constructor(private iframeCommunications: IframeCommunicationService,
        private hoziUtils: HoziUtils) { }

    ngOnInit() {
        this.registerApiHandler();
        this.handleUrlParams();
    }

    registerApiHandler() {
        if (this.iframeCommunications.isEmbededInIframe()) {

            this.iframeCommunications.listen(IframeActions.LOAD_VIDEO, async (event: { data: string }) => {
                if (event.data !== '') {

                    try {
                        this.channelSelected({ channelId: event.data });
                    } catch {
                        throw Error('error while loading video');
                    }
                }
            }, 500);

            this.iframeCommunications.sendToEmbeddingParent(IframeActions.ON_READY);

            this.iframeCommunications.listen(IframeActions.LOAD_VIDEO_BY_IP_AND_ABS_TIME, async (event: { data: { startFrom: number, videoId: string } }) => this.loadVideoByIpAndAbsTime({ channelId: event.data.videoId }))
        }
    }

    handleUrlParams() {
        const searchParams = new URL(window.location.toString()).searchParams;
        const urlParams = window.location.pathname.match(/\/(embd)\/(.*)/);

        this.embedOptions = new EmbedOptions(searchParams);

        const channelId = urlParams && urlParams[2];
        if (channelId) {
            setTimeout(() => {
                this.channelSelected({
                    channelId: channelId
                })
            }, 100)
        }
    }

    toggleView(currentView: CurrentView) {
        this.currentView$.next(this.currentView$.value == currentView ? 'middle' : currentView);
    }

    channelSelected(selectedChannel: SelectedMediaParams) {
        this.currentView$.next('middle');
        this.selectedVideo = { ...selectedChannel }
    }

    loadVideoByIpAndAbsTime(selectedChannel: SelectedMediaParams) {
        this.channelSelected({ ...selectedChannel, startFrom: 3 })
    }
}