import { Component, Output, EventEmitter, OnInit } from "@angular/core";
import { Observable, of } from "rxjs";
import { SelectedMediaParams } from '../app.component'
import { ChannelService } from 'src/shared/services/channels.service'

export interface Channel {
    id: string;
    name: string;
}

@Component({
    selector: 'channels-tab',
    templateUrl: './channels-tab.component.html',
    styleUrls: ['./channels-tab.component.scss']
})
export class ChannelsTabComponent implements OnInit{

    channels$: Observable<Channel[]>;
    @Output() onSelect = new EventEmitter<SelectedMediaParams>();

    constructor(private channelService: ChannelService) {}
    
    ngOnInit() {
        this.channels$ = this.channelService.channels$;
    }

    playVideo(channel: Channel) {
        this.onSelect.emit({ channelId: channel.id });
    }
}