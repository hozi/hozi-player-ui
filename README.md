# Hozi Player UI

## Description
This project containes hozi player ui for embedding.

## Web url
Default website- https://hozi-player-ui.web.app/embd

Full version website -  https://hozi-player-ui.web.app/embd?showFullUI=true

## Api

### Routes

/embd/ -> not playing any channel

/embd/:channelId -> for playing a specific channel - channelId **can only be from 1 to 9**

### Params options

You can add one or more from this options to url as params for controlling the player

1. showFullUI - whether show full ui (with channels list and search) or not (only the player), **false in default**

2. hideVideoControls - whether hide video controllers (bottom bar) or not, **false in default**

3. showVideoName - whether show video name (top bar) or not, **true in default**

## Getting started

First you need to add iframe tag to your html with the web url.

Then, change the src attribute to your need with the optionals params.

### Examples

For full player with search and channels list you should do this -

```<iframe allowfullscreen="true" frameborder="0" scrolling="no" width="500px" height="368px" src="https://hozi-player-ui.web.app/embd?showFullUI=true"></iframe>```

For mini player that nevigate directly to your channel id you should do this -

```<iframe allowfullscreen="true" frameborder="0" scrolling="no" width="500px" height="368px" src="https://hozi-player-ui.web.app/embd/{$channelId}"></iframe>```

For player without any bars and controls you should do this -

```<iframe allowfullscreen="true" frameborder="0" scrolling="no" width="500px" height="368px" src="https://hozi-player-ui.web.app/embd/{$channelId}?hideVideoControls=true&showVideoName=false"></iframe>```


